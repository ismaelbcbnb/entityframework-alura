using ScreenSound.Banco;
using ScreenSound.Modelos;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);
builder.Services.Configure<Microsoft.AspNetCore.Http.Json.JsonOptions>(options => options.SerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

var app = builder.Build();

app.MapGet("/Artistas", () =>
{
    var dal = new DAL<Artista>(new ScreenSoundContext());
    return dal.List();
});

app.MapGet("/Artista/{nome}", (string nome) =>
{
    var dal = new DAL<Artista>(new ScreenSoundContext());
    return dal.ListBy(a => a.Nome.ToUpper().Equals(nome.ToUpper()));
});


app.Run();
